//
//  METableTestViewController.m
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "METableTestViewController.h"
#import "ME3SplitViewController.h"
#import "MEArrowedViewCell.h"

static NSString * arrowedCellID = @"arrowedCellID";

@interface METableTestViewController ()
@end

@implementation METableTestViewController
{
    NSIndexPath *lastSelectedPath;
    NSTimer *updateTimer;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear: %@", self); 
}

- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"viewDidAppear: %@", self); 
}

- (void) update:(NSTimer*)tm{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(update:) userInfo:nil repeats:NO];
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    self.view.clipsToBounds = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
    [self.tableView registerClass:[MEArrowedViewCell class] forCellReuseIdentifier:arrowedCellID];
    
    //updateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(update:) userInfo:nil repeats:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    lastSelectedPath = indexPath;
    
    if ([self.restorationIdentifier isEqualToString:@"leftTable"]) {
        [self performSegueWithIdentifier:ME_3SPLIT_MIDDLE_SEGUE_ID sender:self];        
    }
    else if ([self.restorationIdentifier isEqualToString:@"middleTable"]){
        [self performSegueWithIdentifier:ME_3SPLIT_RIGHT_SEGUE_ID sender:self];        
    }        
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MEArrowedViewCell *cell = [tableView dequeueReusableCellWithIdentifier:arrowedCellID forIndexPath:indexPath];
    
    // Configure the cell...

    cell.textLabel.text = [NSString stringWithFormat:@"Test (%li<) %li (%f)", (long)_row, (long)indexPath.row, [NSDate timeIntervalSinceReferenceDate]];    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    return cell;
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
            
//    NSLog(@"----> %@", segue);
    
    METableTestViewController *nextVC = segue.destinationViewController;    
    nextVC.row = lastSelectedPath.row;
}

 

@end
