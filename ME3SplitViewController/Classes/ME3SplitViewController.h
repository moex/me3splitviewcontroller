//
//  ME3SplitViewController.h
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ME3SplitViewController : UIViewController
@property (nonatomic,strong,readonly) UIView  *leftView, *middleView, *rightView;
@property (nonatomic,strong) UIViewController *leftViewController;
@property (nonatomic,strong) UIViewController *middleViewController;
@property (nonatomic,strong) UIViewController *rightViewController;
@property (nonatomic,strong) NSString  *leftSegueID;

@property (nonatomic, copy) void (^didSplitViewSet)(UIViewController *controller);
@property (nonatomic, copy) void (^didSplitViewUnset)(UIViewController *controller);

@end
