//
//  MEArrowedViewCell.h
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 09.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEArrowedViewCell : UITableViewCell
@property(nonatomic,readonly,strong) UIImageView *devider;
- (void) addDevider;
@end
