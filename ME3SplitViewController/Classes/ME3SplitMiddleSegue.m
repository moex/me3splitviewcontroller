//
//  ME3SplitMiddleSegue.m
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "ME3SplitMiddleSegue.h"
#import "ME3SplitViewController.h"

@implementation ME3SplitMiddleSegue
- (void) perform{
    UIViewController *source = self.sourceViewController;
    ME3SplitViewController *splitVC = (ME3SplitViewController*)source.navigationController.topViewController;        
    UIViewController *dest   = self.destinationViewController;    

    if ([splitVC respondsToSelector:@selector(middleViewController)]) {
        splitVC.middleViewController = dest;        
    }
    else if ([source respondsToSelector:@selector(navigationController)] && source.navigationController){
        [source.navigationController pushViewController:dest animated:YES];
    }
}
@end
