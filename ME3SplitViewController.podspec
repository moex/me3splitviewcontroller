Pod::Spec.new do |s|
  s.name         = "ME3SplitViewController"
  s.version      = "0.1.0"
  s.summary      = "A simple view container for 3 splited views."

  s.homepage     = "https://bitbucket.org/denn_nevera/me3splitviewcontroller.git"

  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }
  s.author       = { "denis svinarchuk" => "denn.nevera@gmail.com" }
  s.platform     = :ios, '8.0'

  s.source       = { :git => "https://bitbucket.org/denn_nevera/me3splitviewcontroller.git", :tag => s.version }
  
  s.source_files  = 'ME3SplitViewController/Classes', 'ME3SplitViewController/Classes/**/*.{h,m}'
  s.public_header_files = 'ME3SplitViewController/Classes/**/*.h'

end
