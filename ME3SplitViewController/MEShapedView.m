//
//  MEShapedView.m
//  METestShapePath
//
//  Created by denis svinarchuk on 09.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEShapedView.h"

@implementation MEShapedView{
    UIImageView *shapedImageView;
}

- (void) __init__{
    
    self.baseWidth = self.bounds.size.height/4;
    self.baseHeight = self.bounds.size.height/2;
    
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.arrowColor = [UIColor colorWithRed:145/255 green:145/255 blue:145/255 alpha:0.5];
    
    shapedImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    shapedImageView.backgroundColor=self.backgroundColor;
    
    [self addSubview:shapedImageView];    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self __init__];
    }
    return self;
}

- (id) initWithImage:(UIImage *)image{
    self = [super initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    if (self) {
        [self __init__];
        self.image = image;
    }
    
    return self;
}

- (void) setImage:(UIImage *)image{
    shapedImageView.image = image;
}

- (CGPoint) addPoint:(CGPoint) point{
    return CGPointMake(point.x+1, point.y+1);
}

- (CGPoint) delPoint:(CGPoint) point{
    return CGPointMake(point.x-1, point.y-1);
}


- (void) redrawContext:(CGContextRef) context maskPath:(CGMutablePathRef) maskPath{
    CGContextMoveToPoint(context, 0, 0);
    
    CGContextAddLineToPoint(context, self.bounds.size.width-self.baseWidth, 0);
    
    CGContextAddLineToPoint(context, self.bounds.size.width-self.baseWidth, 
                            self.bounds.size.height/2.-self.baseHeight/2.);
    
    CGContextAddLineToPoint(context, self.bounds.size.width, 
                            self.bounds.size.height/2.);
    
    CGContextAddLineToPoint(context, self.bounds.size.width-self.baseWidth, 
                            self.bounds.size.height/2.+self.baseHeight/2.);
    
    CGContextAddLineToPoint(context, self.bounds.size.width-self.baseWidth, 
                            self.bounds.size.height);
    
    CGContextAddLineToPoint(context, 0, self.bounds.size.height);
    
    
    CGPathMoveToPoint(maskPath, NULL, 0, 0);
    CGPathAddLineToPoint(maskPath, NULL, self.bounds.size.width-self.baseWidth-0.5, 0);
    
    CGPathAddLineToPoint(maskPath, NULL, self.bounds.size.width-self.baseWidth-0.5, 
                         self.bounds.size.height/2.-self.baseHeight/2.-0.5);
    
    CGPathAddLineToPoint(maskPath, NULL, self.bounds.size.width-0.5, 
                         self.bounds.size.height/2.);
    
    CGPathAddLineToPoint(maskPath, NULL, self.bounds.size.width-self.baseWidth-0.5, 
                         self.bounds.size.height/2.+self.baseHeight/2.);
    
    CGPathAddLineToPoint(maskPath, NULL, self.bounds.size.width-self.baseWidth-0.5, 
                         self.bounds.size.height);
    
    CGPathAddLineToPoint(maskPath, NULL, 0, self.bounds.size.height);

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    NSLog(@"----> DRAW drawRect: %@", self);
    
    // Drawing code
    
    self.clipsToBounds = YES;

    // Get the context
    CGContextRef context = UIGraphicsGetCurrentContext();
    CAShapeLayer *mask = [[CAShapeLayer alloc] init];    
    CGMutablePathRef maskPath = CGPathCreateMutable();
    
    // Pick colors
    CGContextSetStrokeColorWithColor(context, [ self.arrowColor CGColor]);
    CGContextSetFillColorWithColor(context, [self.arrowColor CGColor]);
    
    // Define triangle    
    // Define path
    
    [self redrawContext:context maskPath:maskPath];
        
    mask.path = maskPath;
    
    mask.fillColor = [[UIColor whiteColor] CGColor];
    shapedImageView.layer.mask = mask;
    // Finalize and draw using path
    
    CGPathRelease(maskPath);
    CGContextClosePath(context);
    CGContextStrokePath(context);
    
    
}


@end
