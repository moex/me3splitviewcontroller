//
//  ME3SplitViewController.m
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "ME3SplitViewController.h"

@interface ME3SplitViewController ()
@property (nonatomic,strong) UIView *leftView, *middleView, *rightView;
@end

@implementation ME3SplitViewController
{
    UIViewController *currentLeftController;
    UIViewController *currentMiddleController;
    UIViewController *currentRightController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView{
    [super loadView];
        
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {        
        NSNumber *topoffset =  [NSNumber numberWithInt:UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone?0:0];
        
        _leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        _middleView = [[UIView alloc] initWithFrame:CGRectMake(320, 0, 320, 400)];
        _rightView  = [[UIView alloc] initWithFrame:CGRectMake(640, 0, 320, 400)];
        
        _leftView.translatesAutoresizingMaskIntoConstraints = _middleView.translatesAutoresizingMaskIntoConstraints = _rightView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_rightView];
        [self.view addSubview:_middleView];
        [self.view addSubview:_leftView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[clip1]-0-[clip2]-0-[clip3]-0-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:@{
                                                                                    @"clip1":_leftView,
                                                                                    @"clip2":_middleView,
                                                                                    @"clip3":_rightView,
                                                                                    }]
         ];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(offset)-[clip]-0-|"
                                                                          options:0
                                                                          metrics:@{@"offset":topoffset}
                                                                            views:@{@"clip":_leftView, @"topGuide":self.topLayoutGuide}]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(offset)-[clip]-0-|"
                                                                          options:0
                                                                          metrics:@{@"offset":topoffset}
                                                                            views:@{@"clip":_middleView, @"topGuide":self.topLayoutGuide}]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-(offset)-[clip]-0-|"
                                                                          options:0
                                                                          metrics:@{@"offset":topoffset}
                                                                            views:@{@"clip":_rightView, @"topGuide":self.topLayoutGuide}]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_leftView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_middleView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_middleView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_rightView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];                
    }
}


- (void) setViewController:(UIViewController*)vc sideView:(UIView*)sideView withCurrentViewController:(UIViewController*) currentVC  complete:(void (^)(void))complete{
        
    if (vc==nil) {
        if (currentVC) {
            [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                             animations:^{
                                 if (currentVC) {
                                     currentVC.view.alpha = 0.0;
                                 }
                             }
                             completion:^(BOOL finished) {
                                 if (currentVC) {
                                     [currentVC.view removeFromSuperview];
                                     
                                     if (self->_didSplitViewUnset) {
                                         self->_didSplitViewUnset(currentVC);
                                     }
                                 }
                                 
                                 if (complete) {
                                     complete();
                                 }
                             }
             ];
        }
        return;
    }
    
    if (vc!=nil && vc==currentVC) {
        return;
    }
    
    UIView *view = vc.view;
    
    view.alpha = 0.0;
    
    [self addChildViewController:vc];

    [sideView insertSubview:vc.view atIndex:0];
    
    view.translatesAutoresizingMaskIntoConstraints = NO;            
    [sideView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":view}]];
    [sideView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":view}]];

    if (currentVC) {
        [currentVC willMoveToParentViewController:self];
    }

    if (_didSplitViewSet) {
        _didSplitViewSet(vc);
    }
    
    [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                     animations:^{
                         if (currentVC) {
                             currentVC.view.alpha = 0.0;
                         }
                         view.alpha = 1.0;
                     } 
                     completion:^(BOOL finished) {
                         
                         if (currentVC) {
                             [currentVC.view removeFromSuperview];
                             [currentVC removeFromParentViewController];
                             
                             if (self->_didSplitViewUnset) {
                                 self->_didSplitViewUnset(currentVC);
                             }
                         }
                         
                         if (complete) {
                             complete();
                         }
                         [self didMoveToParentViewController:vc];
                     }
     ];
}

- (void) setLeftViewController:(UIViewController *)vc{
    if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM()) {
        UIViewController *cvc = currentLeftController;
        currentLeftController = vc;
        [self setViewController:vc sideView:_leftView withCurrentViewController:cvc complete:^{}];
    }
    else{
        currentLeftController = vc;        
        [self.navigationController setViewControllers:@[vc] animated:NO];
    }
}

- (void) setMiddleViewController:(UIViewController *)vc{
    if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM()) {
        UIViewController *cvc = currentMiddleController;
        currentMiddleController = vc;
        [self setViewController:vc sideView:_middleView withCurrentViewController:cvc complete:^{}];
    }
    else{
        currentLeftController = vc;        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void) setRightViewController:(UIViewController *)vc{
    if (UIUserInterfaceIdiomPad==UI_USER_INTERFACE_IDIOM()) {
        UIViewController *cvc = currentRightController;
        currentRightController = vc;
        [self setViewController:vc sideView:_rightView withCurrentViewController:cvc complete:^{}];
    }else {
        currentLeftController = vc;        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self performSegueWithIdentifier:self.leftSegueID sender:self];
}

- (void) viewDidLoad{
    [super viewDidLoad];
    if ([self leftSegueID]==nil) {
        [NSException raise:@"Invalid left split segue" format:@"ME3SplitViewController left split view segue has not found"];
    }
}

@end
