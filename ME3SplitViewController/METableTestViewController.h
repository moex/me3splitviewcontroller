//
//  METableTestViewController.h
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>


#define ME_3SPLIT_LEFT_SEGUE_ID @"3SplitLeftSegue"
#define ME_3SPLIT_MIDDLE_SEGUE_ID @"3SplitMiddleSegue"
#define ME_3SPLIT_RIGHT_SEGUE_ID @"3SplitRightSegue"

@interface METableTestViewController : UITableViewController

@property(nonatomic,assign) NSInteger row; 

@end
