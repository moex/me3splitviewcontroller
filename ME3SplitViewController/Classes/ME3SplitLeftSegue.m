//
//  ME3SplitLeftSegue.m
//  ME3SplitViewController
//
//  Created by denis svinarchuk on 07.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "ME3SplitLeftSegue.h"
#import "ME3SplitViewController.h"

@implementation ME3SplitLeftSegue

- (void) perform{
    ME3SplitViewController *source = self.sourceViewController;
    UIViewController *dest   = self.destinationViewController;    
    source.leftViewController = dest;    
}

@end
